import { useState } from 'react';
import * as C from './App.styles';
import { AddTask } from './components/AddTask';
import { ListItem } from './components/ListItem';

import { Item } from './types/Item';

const App = () => {
  const [list, setList] = useState<Item[]>([]);

  const handleAddTask = (taskName: string) => {
    setList([
      ...list,
      {
        id: list.length + 1,
        name: taskName,
        done: false,
      },
    ]);
  };

  return (
    <C.Container>
      <C.Area>
        <C.Header>Lista de Tarefas</C.Header>

        <AddTask onEnter={handleAddTask} />

        {list.map((item, index) => (
          <ListItem key={index} item={item} />
        ))}
      </C.Area>
    </C.Container>
  );
};

export default App;
